# Spark App
Compile application:
```sh
$ sbt compile
```
Make package
```sh
$ sbt package
```
Run with Spark
```sh
$ SPARK_PATH/bin/spark-submit --master local[4] target/scala-2.10/spark_app_2.10-0.1.jar --top 10 --country "United States" PATH_TO_INPUT_FILE/input.csv
```
