import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import java.nio.file.{Paths, Files}

object Spark_App {
//For parsing options
 type OptionMap = Map[Symbol, String]
 def nextOption(map : OptionMap, list: List[String]) : OptionMap = {
      def isSwitch(s : String) = (s(0) == '-')
      list match {
        case Nil => map
        case "--country" :: string :: tail =>
                               nextOption(map ++ Map('country -> string), tail)
        case "--top" :: string :: tail =>
                               nextOption(map ++ Map('top -> string), tail)
        case string :: opt2 :: tail if isSwitch(opt2) => 
                               nextOption(map ++ Map('infile -> string), list.tail)
        case string :: Nil =>  nextOption(map ++ Map('infile -> string), list.tail)
        case option :: tail => println("Unknown option "+option) 
                               exit(1) 
      }
  }
// Match string option
 def getOptionByKey(map : OptionMap, key : Symbol) : String = {
     val value = map.get(key) 
     value match {
       case Some(opt:String) => opt // this extracts the value in a as an Int
       case _ => ""
     }
  }

// Main finction
 def main(args: Array[String]) {
  val usage = "Use argument option [--country \"COUNTRY\"] [--top INT] filename"
// Check args
  if (args.length == 0) 
    {
      println(usage)
      exit(1)
    }
// Get options 
  val options = nextOption(Map(),args.toList)
  val filename = getOptionByKey(options,'infile)
  val top = getOptionByKey(options,'top)
  val country = getOptionByKey(options,'country)
// Check CSV file
  if (!Files.exists(Paths.get(filename)))
	{
	println("Input file: "+filename+" is unavalible... Exit")
	System.exit(1)
	}
//Create local Spark context
  val sc = new SparkContext("local", "Spark_App")
//Read and map CSV to RDD
   val pairs = sc.textFile(filename).cache()
             .map(line => line.split(",").map(_.trim))
	     .map(line => (line(0), line(1)))
//Group authors by name and count list of country is valid 
  val groupPairs = pairs.groupByKey()
    	         .map(line => (line._1, line._2.count((i: String) => i == country)))
//And filter invalid authors
  val filteredPairs = groupPairs.filter{case (author, _) => author != ""}		
                    .sortBy(_._2, false)
  filteredPairs.take(top.toInt).foreach(println)
  }
}
