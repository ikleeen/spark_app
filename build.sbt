name := "spark_app"
version := "0.1"
scalaVersion := "2.10.4"
val sparkVersion = "1.4.0"
val sparkDependencyScope = "provided"
libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion % sparkDependencyScope
)

	
